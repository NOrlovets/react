import React from 'react';
import './App.css';
import DisplayList from './DisplayList';
import { confirmAlert } from 'react-confirm-alert'; // Import 
import './react-confirm-alert.css' // Import css 
import Progress from './Progress';

var rand = require('random-key');

export default class App extends React.Component {

  constructor() {
    super();
    this.state = {
      title: '', todos: [],
      progress: 0,
      goal: 0,
    };
  }

  handleDone(idToBeMarkedAsDone) {
    var _todos = this.state.todos;
    var todo = _todos.filter((todo) => {
      return todo.id === idToBeMarkedAsDone;
    })[0];

    todo.done = !todo.done;

    this.setState({ todos: _todos });
    this.setState({
      progress: this.state.todos.filter((todo) => { return todo.done }).length
    });
  }

  handleDelete(idToBeDeleted) {
    var newTodos = this.state.todos.filter((todo) => {
      return todo.id !== idToBeDeleted
    })
    var newTodo = this.state.todos.filter((todo) => {
      return todo.id === idToBeDeleted
    })
    confirmAlert({
      title: 'Confirm to submit',                        // Title dialog 
      message: 'Are you sure to do this.',               // Message dialog 
      confirmLabel: 'Confirm',                           // Text button confirm 
      cancelLabel: 'Cancel',                             // Text button cancel 
      onConfirm: () =>
        (!newTodo[0].done) ? this.setState({ todos: newTodos, goal: this.state.goal - 1 })
          : this.setState({ todos: newTodos, goal: this.state.goal - 1, progress: this.state.progress - 1 }),
      onCancel: () => null,
    })
  }

  handleSubmit(event) {
    event.preventDefault();

    var title = this.state.title;
    var newTodos = this.state.todos.concat({
      title: title,
      id: rand.generate(),
      done: false
    });
    this.setState({ title: '', todos: newTodos, goal: this.state.todos.length + 1 });
    //this.setState({  });
  }

  handleChange(event) {
    var title = event.target.value;
    this.setState({ title: title });
  }


  render() {
    return <div>
      <h1> TODO </h1>
      <div className="progress-container">
        <Progress
          progress={this.state.progress}
          goal={this.state.goal} />
      </div>
      <form onSubmit={this.handleSubmit.bind(this)}>
        <input type="text"
          onChange={this.handleChange.bind(this)}
          value={this.state.title} />
      </form>

      <DisplayList
        handleDone={this.handleDone.bind(this)}
        handleDelete={this.handleDelete.bind(this)}
        todos={this.state.todos} />
      {/* <p>{this.state.goal}</p>
      <p>{this.state.progress}</p> */}
    </div>;
  }
}

// class ProgressBar extends React.Component {
//   calculateProgress(progress, goal) {
//     if (Number(progress) === 0) {
//       return 0.75 + "%"
//     }
//     if (Number(goal) >= Number(progress)) {
//       return (progress / goal) * 100 + "%"
//     } else {
//       return 100 + "%"
//     }
//   }

//   render() {
//     const { progress, goal } = this.props
//     return (
//       <div
//         className="progress-bar"
//         style={{ width: this.calculateProgress(progress, goal) }}
//       ></div>
//     )
//   }
// }